import pandas as pd
import mlflow
import os
from dotenv import load_dotenv
from fastapi import FastAPI, File, UploadFile, HTTPException

load_dotenv()

app = FastAPI()

if os.getenv("AWS_ACCESS_KEY_ID") is None or os.getenv("AWS_SECRET_ACCESS_KEY") is None:
    exit(17)


class Model:
    def __init__(self, model_name, model_stage):
        # self.model = mlflow.pyfunc.load_model(f"models:/{model_name}/{model_stage}")

        model_uri = "s3://arts/1/e81cddc27dce49cc88de728605a7aca2/artifacts/model"
        self.model = mlflow.sklearn.load_model(model_uri)
        print(self.model)
        # self.model = mlflow.pyfunc.load_model("")

    def predict(self, data):
        predictions = self.model.predict(data)
        return predictions


model = Model("registered_model", "Staging")


@app.post("/invocations")
async def create_upload_file(file: UploadFile = File(...)):
    if file.filename.endswith(".csv"):
        with open(file.filename, "wb") as f:
            f.write(file.file.read())
        data = pd.read_csv(file.filename)
        os.remove(file.filename)

        return model.predict(data)
    else:
        raise HTTPException(
            status_code=400, detail="Invalid file format. Only CSV files accepted."
        )
