import src


path_to_deals = "data/raw/deals.csv"
path_to_resampled_dict = "data/interim/resampled_dict.pkl"
path_to_minmax_dict = "data/interim/minmaxed_resampled_dict.pkl"
path_to_featured_df = "data/interim/featured_resampled_df.pkl"
path_to_final_df = "data/interim/final_df.pkl"


if __name__ == "__main__":
    src.save_resampled_dict(
        [path_to_deals, path_to_resampled_dict], standalone_mode=False
    )
    src.set_correct_ohlc_min_max(
        [path_to_resampled_dict, path_to_minmax_dict], standalone_mode=False
    )
    src.add_features_to_resampled_dict(
        [path_to_minmax_dict, path_to_featured_df], standalone_mode=False
    )
    src.merge_deals_with_features(
        [path_to_deals, path_to_featured_df, path_to_final_df], standalone_mode=False
    )
