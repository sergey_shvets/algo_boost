from setuptools import find_packages, setup  # type: ignore

setup(
    name="src",
    packages=find_packages(),
    version="0.1.0",
    description="Machine learning optimization for trading signals with Python",
    author="Sergey Shvets",
    license="BSD-3",
)
