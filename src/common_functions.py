import pickle
import os
from typing import Any


def save_pickle(pickle_data: Any, pickle_path: str) -> None:
    """
    Save pickle data to pickle_path

    :param pickle_data: data to save
    :param pickle_path: path to save
    :return: None
    """

    with open(pickle_path, "wb") as fp:
        pickle.dump(pickle_data, fp)


def load_pickle(pickle_path: str) -> Any:
    """
    Load pickle data from pickle_path

    :param pickle_path: path to load
    :return: loaded data
    """

    if os.path.isfile(pickle_path):
        with open(pickle_path, "rb") as fp:
            pickle_data = pickle.load(fp)
            return pickle_data
    else:
        raise FileNotFoundError(f"File {pickle_path} not found")
