import os
import click
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import (
    accuracy_score,
    precision_score,
    recall_score,
    f1_score,
)
from jinja2 import Environment, FileSystemLoader

from src.logging_config import log
import src.common_functions as cf
import src.constants as constants


def get_score_value_for_thr(
    proba_thr_: float, y_: np.ndarray, y_pred_proba_: np.ndarray, scorer
) -> float:
    """
    Get score value for threshold

    Parameters
    ----------
    proba_thr_ : float
        Threshold
    y_ : np.ndarray
        Target array
    y_pred_proba_ : np.ndarray
        Predicted probabilities array
    scorer : callable
        Scorer function

    Returns
    -------
    float
    """
    y_pred_ = np.where(y_pred_proba_ >= proba_thr_, 1, 0)

    return scorer(y_, y_pred_)


def get_trading_scores_for_thr(
    proba_thr_: float, y_pred_proba_: np.ndarray, pnl_percent_: np.ndarray
) -> tuple[float, float, float]:
    """
    Get trading metrics for threshold

    Parameters
    ----------
    proba_thr_ : float
        Threshold
    y_pred_proba_ : np.ndarray
        Predicted probabilities array
    pnl_percent_ : np.ndarray
        PnL percent array

    Returns
    -------
    tuple[float, float, float]
        "winrate", "sum_pnl", "num_trades"
    """
    y_pred_ = np.where(y_pred_proba_ >= proba_thr_, 1, 0)

    hstacked = np.hstack((pnl_percent_.reshape(-1, 1), y_pred_.reshape(-1, 1)))
    cls_df = pd.DataFrame(hstacked, columns=["pnl_percent", "cls"])
    selected_cls_df = cls_df[cls_df["cls"] == 1]

    winrate = (
        selected_cls_df[selected_cls_df["pnl_percent"] > 0].shape[0]
        / selected_cls_df.shape[0]
    )
    sum_pnl = selected_cls_df["pnl_percent"].sum()
    num_trades = selected_cls_df.shape[0]

    return winrate, sum_pnl, num_trades


@click.command()
@click.argument("path_to_model", type=click.Path(exists=True))
@click.argument("path_to_report", type=click.Path())
def make_html_report(path_to_model: str, path_to_report: str) -> None:
    """
    Create HTML report

    Parameters
    ----------
    path_to_model : str
        Path to model
    path_to_report : str
        Path to report

    Returns
    -------
    None
    """
    log.debug("Creating report")

    # Create figures for test set
    create_figures(path_to_model, "test")

    # Create figures for train set
    create_figures(path_to_model, "train")

    # Load the template
    env = Environment(loader=FileSystemLoader(constants.ROOT_DIR))

    template = env.get_template("reports/report_template.html")

    # Define the data to fill in the template
    data = {
        "title": "Model report",
        "text": "Classification and trading metrics",
        "image_path_accuracy_train": "figures/accuracy_score_train.png",
        "image_path_accuracy_test": "figures/accuracy_score_test.png",
        "image_path_precision_train": "figures/precision_score_train.png",
        "image_path_precision_test": "figures/precision_score_test.png",
        "image_path_recall_train": "figures/recall_score_train.png",
        "image_path_recall_test": "figures/recall_score_test.png",
        "image_path_f1_train": "figures/f1_score_train.png",
        "image_path_f1_test": "figures/f1_score_test.png",
        "image_path_winrate_train": "figures/winrate_train.png",
        "image_path_winrate_test": "figures/winrate_test.png",
        "image_path_sum_pnl_train": "figures/sum_pnl_train.png",
        "image_path_sum_pnl_test": "figures/sum_pnl_test.png",
        "image_path_num_trades_train": "figures/num_trades_train.png",
        "image_path_num_trades_test": "figures/num_trades_test.png",
    }

    # Render the template with the data
    html = template.render(data)

    # Save the rendered HTML to a file
    with open(path_to_report, "w") as f:
        f.write(html)

    log.debug("Report created")


def create_figures(path_to_model: str, stage: str) -> None:
    """
    Create figures for stage set

    Parameters
    ----------
    path_to_model : str
        Path to model
    stage : str
        Stage of data (train or test)

    Returns
    -------
    None
    """
    log.debug(f"Creating figures for {stage} set")

    # Load model from file
    model = cf.load_pickle(path_to_model)

    # Load data from file
    df = pd.read_pickle(
        os.path.join(constants.ROOT_DIR, "data", "processed", f"{stage}_df.pkl")
    )

    # Get X and y, pnl_percent to check trading metrics
    y = df["cls"]
    pnl_percent = df["pnl_percent"].values
    X = df.drop(["cls", "pnl_percent"], axis=1)

    # Get predicted probabilities
    y_pred_proba = model.predict_proba(X)[:, 1]

    # Get list of thresholds
    proba_thr_list = np.linspace(y_pred_proba.min(), y_pred_proba.max(), 20)

    # Get trading metrics df
    trade_metrics_df = pd.DataFrame(proba_thr_list, columns=["proba_thr"])
    trade_metrics_list = ["winrate", "sum_pnl", "num_trades"]
    trade_metrics_df[trade_metrics_list] = trade_metrics_df[["proba_thr"]].apply(
        lambda x: pd.Series(
            get_trading_scores_for_thr(x["proba_thr"], y_pred_proba, pnl_percent)
        ),
        axis=1,
    )

    for trade_metric in trade_metrics_list:
        plt.plot(trade_metrics_df["proba_thr"], trade_metrics_df[trade_metric])
        plt.title(f"{trade_metric} {stage}")
        path_to_figure = os.path.join(
            constants.ROOT_DIR,
            "reports",
            "figures",
            f"{trade_metric}_{stage}.png",
        )
        plt.savefig(path_to_figure)
        plt.clf()

    # Get classification metrics df
    score_func_list = [
        accuracy_score,
        precision_score,
        recall_score,
        f1_score,
    ]

    for score_func in score_func_list:
        metrics_df = pd.DataFrame(proba_thr_list, columns=["proba_thr"])
        metrics_df["score"] = metrics_df["proba_thr"].apply(
            get_score_value_for_thr, args=(y, y_pred_proba, score_func)
        )

        plt.plot(metrics_df["proba_thr"], metrics_df["score"])
        plt.title(f"{score_func.__name__} {stage}")
        path_to_figure = os.path.join(
            constants.ROOT_DIR,
            "reports",
            "figures",
            f"{score_func.__name__}_{stage}.png",
        )
        plt.savefig(path_to_figure)
        plt.clf()


if __name__ == "__main__":
    # make_html_report([
    #     "../models/rf_model.pkl",
    #     "../reports/report.html"
    # ])
    make_html_report()
