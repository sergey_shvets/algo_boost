from google.oauth2 import service_account  # type: ignore
from googleapiclient.discovery import build  # type: ignore
from googleapiclient.errors import HttpError  # type: ignore
import pickle

import src.constants as constants


class DriveAPI:
    def __init__(self, credentials_path=f"{constants.ROOT_DIR}/gdrive_creds.json"):
        scopes = ["https://www.googleapis.com/auth/drive"]
        self.credentials = service_account.Credentials.from_service_account_file(
            credentials_path, scopes=scopes
        )
        self.service = build("drive", "v3", credentials=self.credentials)

    def list_files_in_folder(self, folder_id: str) -> list[dict]:
        files = []
        page_token = None
        query = f"'{folder_id}' in parents and trashed = false"
        while True:
            response = (
                self.service.files()
                .list(
                    q=query,
                    fields="nextPageToken, files(id,name)",
                    pageToken=page_token,
                )
                .execute()
            )
            files.extend(response.get("files", []))
            page_token = response.get("nextPageToken", None)
            if page_token is None:
                break

        return files

    def download_and_unpickle_file(self, file_id):
        loaded_object = None

        try:
            file_content = self.service.files().get_media(fileId=file_id).execute()
        except HttpError as error:
            print(f"An error occurred: {error}")
            file_content = None

        # Load the file using pickle
        if file_content:
            loaded_object = pickle.loads(file_content)

        return loaded_object
