import click

import src.common_functions as cf
from src.logging_config import log


@click.command()
@click.argument("path_to_resampled_dict", type=click.Path(exists=True))
@click.argument("path_to_minmax_dict", type=click.Path())
def set_correct_ohlc_min_max(
    path_to_resampled_dict: str, path_to_minmax_dict: str
) -> None:
    """
    Correct high and low values in ohlc dataframes from dict
    """
    log.debug("Started set_correct_ohlc_min_max()")

    resampled_dict = cf.load_pickle(path_to_resampled_dict)

    # Correct high and low values
    for symbol, resample_dict in resampled_dict.items():
        for resample_tf, resample_df in resample_dict.items():
            resample_df["high"] = resample_df[["open", "high", "low", "close"]].max(
                axis=1
            )
            resample_df["low"] = resample_df[["open", "high", "low", "close"]].min(
                axis=1
            )
            resampled_dict[symbol][resample_tf] = resample_df

    cf.save_pickle(resampled_dict, path_to_minmax_dict)

    log.debug("Finished set_correct_ohlc_min_max()")


if __name__ == "__main__":
    # set_correct_ohlc_min_max([
    #     "../data/interim/resampled_dict.pkl",
    #     "../data/interim/minmaxed_resampled_dict.pkl"
    # ])
    set_correct_ohlc_min_max()
