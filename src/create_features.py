import click
import numpy as np
import pandas as pd
import multiprocessing

from src.logging_config import log
import src.common_functions as cf


def get_features_df(symbol: str, resample_dict: dict, windows: tuple) -> pd.DataFrame:
    """
    Get features dataframe

    :param symbol: symbol
    :param resample_dict: resampled dict
    :param windows: windows
    :return: features dataframe
    """
    log.debug(f"Getting features dataframe for symbol [{symbol}]")

    return_fdf = None

    for resample_tf, resample_df in resample_dict.items():
        fdf = resample_df.copy()

        fdf["vol_avg_win_0"] = fdf["volume"].rolling(windows[0]).mean().shift(1)
        fdf["vol_max_win_0"] = fdf["volume"].rolling(windows[0]).max().shift(1)

        fdf["signal_date"] = fdf.index
        # fdf["weekday"] = fdf.index.day_name()

        fdf["hour"] = fdf.index.hour

        fdf["symbol"] = symbol
        fdf["timeframe"] = resample_tf

        fdf["direction_last_bar"] = np.where(fdf["close"] > fdf["open"], 1, -1)

        for w in windows:
            fdf[f"vol_avg_win_{w}_ratio"] = (
                fdf["volume"].rolling(w).mean().shift(1) / fdf["volume"]
            )

        fdf["return_last_bar"] = (fdf["close"] - fdf["open"]) / fdf["open"]

        for w in windows:
            fdf[f"return_win_{w}_bars"] = (
                fdf["close"].shift(1) - fdf["open"].shift(w)
            ) / fdf["open"].shift(w)

        fdf["volatility_last_bar"] = (fdf["high"] - fdf["low"]) / fdf["close"]

        for w in windows:
            fdf[f"volatility_win_{w}_bars"] = (
                fdf["high"].shift(1) - fdf["low"].shift(w)
            ) / fdf["close"].shift(w)

        if return_fdf is None:
            return_fdf = fdf
        else:
            return_fdf = pd.concat([return_fdf, fdf])

    return return_fdf


@click.command()
@click.argument("path_to_minmax_dict", type=click.Path(exists=True))
@click.argument("path_to_featured_df", type=click.Path())
def add_features_to_resampled_dict(
    path_to_minmax_dict: str, path_to_featured_df: str
) -> None:
    """
    Add features to resampled dict

    :param path_to_minmax_dict: path to minmaxed resampled dict
    :param path_to_featured_df: path to featured resampled dict
    """
    resampled_dict = cf.load_pickle(path_to_minmax_dict)

    windows = (3, 5, 7)

    log.debug(f"Adding features to resampled dict with windows [{windows}]")

    params_list = [
        (symbol, resample_dict, windows)
        for symbol, resample_dict in resampled_dict.items()
    ]

    with multiprocessing.Pool() as pool:
        result_list = pool.starmap(get_features_df, params_list)

    feature_df = pd.concat(result_list)

    cf.save_pickle(feature_df, path_to_featured_df)

    log.debug(f"Saved featured df to {path_to_featured_df}")


if __name__ == "__main__":
    # add_features_to_resampled_dict(
    #     [
    #         "../data/interim/minmaxed_resampled_dict.pkl",
    #         "../data/interim/featured_resampled_df.pkl",
    #     ]
    # )
    add_features_to_resampled_dict()
