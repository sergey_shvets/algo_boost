import logging


def setup_logger(name):
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        "-%(asctime)s [%(levelname)s][%(filename)s][%(funcName)s] %(message)s"
    )
    handler.setFormatter(formatter)

    logger.addHandler(handler)

    return logger


log = setup_logger("src")
