import os
import click
import pandas as pd

# from sklearn.utils import shuffle

import src.constants as constants
from src.logging_config import log


@click.command()
@click.argument("path_to_final_df", type=click.Path())
@click.argument("test_size", type=click.FLOAT)
@click.argument("pnl_thr", type=click.FLOAT)
def make_train_test_split(path_to_final_df: str, test_size: float, pnl_thr: float):
    log.debug("Started making train test split")

    # Load data
    df = pd.read_pickle(path_to_final_df)

    # Set 'cls' column as target
    df["cls"] = df["pnl_percent"].apply(lambda x: 1 if x >= pnl_thr else 0)

    # TIMEBASED SPLIT
    # Get a timestamp separator for train test split
    train_test_ts = pd.Timestamp("2021-07-01").timestamp()

    # Make a train test split by datetime. Separate test set by date 1 July 2021
    train = df[df["enter_signal_ts"] < train_test_ts]
    test = df[df["enter_signal_ts"] >= train_test_ts]

    # # SHUFFLE SPLIT
    # shuffled_df = shuffle(df, random_state=42)
    #
    # # Get separator index
    # test_index = int(len(shuffled_df) * (1 - test_size))
    #
    # # Split data
    # train = shuffled_df.iloc[:test_index]
    # test = shuffled_df.iloc[test_index:]

    log.debug(f"Train set size: {len(train)}, test set size: {len(test)}")

    # Save data
    train.to_pickle(
        os.path.join(constants.ROOT_DIR, "data", "processed", "train_df.pkl")
    )
    test.to_pickle(os.path.join(constants.ROOT_DIR, "data", "processed", "test_df.pkl"))

    log.debug("Finished making train test split")


if __name__ == "__main__":
    # make_train_test_split(
    #     [
    #         "../data/processed/final_df.pkl",
    #         "0.3",
    #         "0.5",
    #     ]
    # )
    make_train_test_split()
