import click
import numpy as np
import pandas as pd

from src.logging_config import log
import src.common_functions as cf


def clean_deals_csv(deals_df: pd.DataFrame) -> pd.DataFrame:
    """
    Clean deals csv

    :param deals_df: deals df
    :return: cleaned deals df
    """
    mandatory_fields = [
        "enter_signal_ts",
        "pnl_percent",
        "symbol",
        "strategy_id",
        "timeframe",
    ]
    deals_df = deals_df[mandatory_fields].copy()

    deals_df["enter_signal_ts"] = pd.to_datetime(deals_df["enter_signal_ts"])

    return deals_df


def post_process_features(df: pd.DataFrame) -> pd.DataFrame:
    # Remove signal_date column (duplicate of enter_signal_ts)
    df = df.drop(["signal_date"], axis=1)

    # Convert enter_signal_ts to int64
    df["enter_signal_ts"] = df["enter_signal_ts"].astype(np.int64) / 10**9

    # One-hot encode symbol
    df = pd.get_dummies(df, columns=["symbol"])

    rows_before_drop = df.shape[0]

    # Remove inf
    df.replace([np.inf, -np.inf], np.nan, inplace=True)

    # Remove nans
    df.dropna(inplace=True)

    log.debug(f"Dropped {rows_before_drop - df.shape[0]} rows due to nans")

    return df


@click.command()
@click.argument("path_to_deals", type=click.Path(exists=True))
@click.argument("path_to_featured_df", type=click.Path(exists=True))
@click.argument("path_to_final_df", type=click.Path())
def merge_deals_with_features(
    path_to_deals: str, path_to_featured_df: str, path_to_final_df: str
) -> None:
    """
    Merge deals with features

    :param path_to_deals: path to deals
    :param path_to_featured_df: path to featured df
    :param path_to_final_df: path to final df
    """
    log.debug("Started merging deals with features")

    deals_df = pd.read_csv(path_to_deals, index_col=0)
    featured_df = pd.read_pickle(path_to_featured_df)

    deals_df = clean_deals_csv(deals_df)

    deals_df["enter_signal_ts"] = pd.to_datetime(deals_df["enter_signal_ts"])
    deals_df["signal_date"] = deals_df["enter_signal_ts"] - pd.to_timedelta(
        deals_df["timeframe"], unit="m"
    )

    final_df = pd.merge(
        deals_df,
        featured_df,
        how="left",
        left_on=["signal_date", "symbol", "timeframe"],
        right_on=["signal_date", "symbol", "timeframe"],
    )

    # Post process features
    final_df = post_process_features(final_df)

    # Save final df
    cf.save_pickle(final_df, path_to_final_df)

    log.debug(f"Saved final df to {path_to_final_df}")


if __name__ == "__main__":
    # merge_deals_with_features(
    #     [
    #         "../data/raw/deals.csv",
    #         "../data/interim/featured_resampled_df.pkl",
    #         "../data/processed/final_df.pkl",
    #     ]
    # )
    merge_deals_with_features()
