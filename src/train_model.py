import os

import click
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import (
    precision_recall_curve,
    auc,
)
from sklearn.pipeline import Pipeline
import mlflow
from mlflow.models.signature import infer_signature

from dotenv import load_dotenv

from src.logging_config import log
import src.common_functions as cf

load_dotenv()

mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))
mlflow.set_experiment("algo_boost_rfc")


def precision_recall_auc_scorer(estimator, X, y) -> float:
    """
    Calculate precision-recall AUC for grid search

    Parameters
    ----------
    estimator : sklearn estimator
        Estimator object
    X : pd.DataFrame
        Features dataframe
    y : pd.Series
        Target series

    Returns
    -------
    float
    """
    y_pred_proba_ = estimator.predict_proba(X)[:, 1]
    precision, recall, _ = precision_recall_curve(y, y_pred_proba_)
    pr_auc = auc(recall, precision)

    return pr_auc


@click.command()
@click.argument("path_to_train_df", type=click.Path(exists=True))
@click.argument("path_to_model", type=click.Path())
def train_model(path_to_train_df: str, path_to_model: str) -> None:
    """
    Train model and save it to pickle file

    Parameters
    ----------
    path_to_train_df : str
        Path to train dataframe
    path_to_model : str
        Path to save model

    Returns
    -------
    None
    """
    log.debug("Start training model")

    # Load data
    df = pd.read_pickle(path_to_train_df)

    y = df["cls"]
    X = df.drop(["cls", "pnl_percent"], axis=1)

    # Create pipeline
    pipe = Pipeline([("rf", RandomForestClassifier(random_state=42))])

    # Set parameters for grid search
    param_grid = {
        "rf__n_estimators": [100, 300, 500],
        "rf__max_depth": [2, 3, 4],
        # "rf__min_samples_split": [2, 3, 4, 5],
        # "rf__min_samples_leaf": [1, 2, 3, 4, 5]
    }

    grid = GridSearchCV(
        pipe, param_grid, cv=5, scoring=precision_recall_auc_scorer, n_jobs=-1
    )

    # Fit grid search object to training data
    grid.fit(X, y)

    # Use the model to make predictions on the test dataset.
    predictions = grid.predict(X)

    signature = infer_signature(X, predictions)
    mlflow.sklearn.log_model(grid, "algo_boost_rfc", signature=signature)

    # Register the model
    model_uri = "runs:/{}/my_model".format(mlflow.active_run().info.run_id)
    mlflow.register_model(model_uri, "registered_model")

    # Save best estimator to pickle file
    cf.save_pickle(grid.best_estimator_, path_to_model)

    log.debug(f"Best params: {grid.best_params_}")

    log.debug(f"Finish training model. Best score: {grid.best_score_:.2f}. Model saved")


if __name__ == "__main__":
    # mlflow.autolog()
    # with mlflow.start_run():
    #     train_model(["../data/processed/train_df.pkl", "../models/rf_model.pkl"])
    mlflow.sklearn.autolog()
    with mlflow.start_run():
        train_model()
