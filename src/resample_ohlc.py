import pandas as pd  # type: ignore
import multiprocessing
import os
import click

import src.common_functions as cf
import src.constants as constants
from src.google_drive_api import DriveAPI
from src.logging_config import log


def get_symbols_and_tfs_from_deals(path_to_deals: str) -> list:
    """
    Get list of symbols and timeframes from deals.csv

    :return: list of lists, where each list contains symbol and list of tfs for this symbol
    """
    log.debug(f"Started reading deals from {path_to_deals}")

    if os.path.isfile(path_to_deals):
        deals_df = pd.read_csv(path_to_deals, index_col=0)
        symbols_and_tfs_dict = (
            deals_df.groupby("symbol")["timeframe"].apply(lambda x: set(x)).to_dict()
        )
        symbols_and_tfs_list = [
            [symbol, list(tfs)] for symbol, tfs in symbols_and_tfs_dict.items()
        ]
    else:
        raise FileNotFoundError(f"File {path_to_deals} not found")

    log.debug(f"Finished reading deals from {path_to_deals}")

    return symbols_and_tfs_list


def get_resampled_ohlcv(
    symbol: str, resample_tfs: list[int]
) -> tuple[str, dict[int, pd.DataFrame]]:
    """
    Get resampled ohlcv dataframes for symbol

    :param symbol: symbol name
    :param resample_tfs: list of resample timeframes
    :return: dict of resampled ohlcv dataframes
    """
    drive_api = DriveAPI()

    ohlc_files_list = drive_api.list_files_in_folder(constants.MINS_DIR_ID_AT_GDRIVE)
    ohlc_file_id = [
        file["id"] for file in ohlc_files_list if file["name"] == symbol + ".pkl"
    ][0]

    ohlc_1t_df = drive_api.download_and_unpickle_file(ohlc_file_id)

    return_dict = {}
    for resample_tf in resample_tfs:
        resample_df = ohlc_1t_df.resample(
            str(resample_tf) + "Min",
            closed="left",
            label="left",
            origin="01.01.2015 00:00:00",
        ).agg(
            {
                "open": "first",
                "high": "max",
                "low": "min",
                "close": "last",
                "volume": "sum",
            }
        )

        # Cut last row if it is not full
        if resample_tf > 1:
            if ohlc_1t_df.last_valid_index() != resample_df.last_valid_index():
                resample_df = resample_df[:-1]

        return_dict[resample_tf] = resample_df

    log.debug(f"Finished resampling {symbol}")

    return symbol, return_dict


@click.command()
@click.argument("path_to_deals", type=click.Path(exists=True))
@click.argument("path_to_resampled_dict", type=click.Path())
def save_resampled_dict(path_to_deals: str, path_to_resampled_dict: str) -> None:
    """
    Save resampled ohlcv dataframes to pickle

    :param path_to_deals: path to deals.csv
    :param path_to_resampled_dict: path to save resampled dict
    :return: None
    """
    log.debug("Started saving resampled dict")

    symbols_tf_list = get_symbols_and_tfs_from_deals(path_to_deals=path_to_deals)
    resampled_dict: dict[str, dict[int, pd.DataFrame]] = {}

    # TODO: remove limit on production
    # symbols_tf_list = symbols_tf_list[:4]

    log.debug(f"Started pool workers for [{len(symbols_tf_list)}] symbols")

    with multiprocessing.Pool() as pool:
        result_list = pool.starmap(get_resampled_ohlcv, symbols_tf_list)

    for symbol, return_dict in result_list:
        for resample_tf, resample_df in return_dict.items():
            if symbol not in resampled_dict:
                resampled_dict[symbol] = {}
            resampled_dict[symbol][resample_tf] = resample_df

    cf.save_pickle(resampled_dict, path_to_resampled_dict)

    log.debug("Finished saving resampled dict")


if __name__ == "__main__":
    # save_resampled_dict([
    #     "../data/raw/deals.csv",
    #     "../data/interim/resampled_dict.pkl"
    # ])
    save_resampled_dict()
